<?php

class RecipeCollection extends \Phalcon\Mvc\Micro\Collection
{
    public function __construct()
    {
        $this->setHandler('RecipeController', true);
        $this->setPrefix('/recipes');

        $this->get('/', 'All');
        $this->get('/{recipeId}', 'Find');
    }
}
