<?php

return [

    'debugMode' => 1, // 0; no developer messages // 1; developer messages and CoreExceptions
    'hostName' => 'http://recipe.localhost',
    'clientHostName' => 'http://recipe.localhost',
    'database' => [

        // Change to your own configuration
        'adapter' => 'Mysql',
        'host' => 'tasteshop.cjsmrnsysrsq.ap-northeast-2.rds.amazonaws.com',
        'username' => 'tasteshop_user',
        'password' => '$2a$12$vMKMN35aZHHPDW4rGen1a.aX0xCa365uls0NvqQP9AYPolK1FFRvu',
        'name' => 'crawling_data',
    ]
];