<?php

use App\Constants\Services as AppServices;

/**
 * @resource("User")
 */
class RecipeController extends \App\Mvc\Controller
{
    public function All()
    {
        $query = $this->di->get( AppServices::QUERY);

        if( $query->getLimit() === null)
            $query->setLimit( 50);

        $phqlQueryParser = $this->di->get( AppServices::PHQL_QUERY_PARSER);

        $phqlBuilder = $phqlQueryParser->fromQuery( $query);
        $phqlBuilder->from( 'Recipe');
        // $phqlBuilder->columns( $query->getFields());

        // Resultset
        $recipes = $phqlBuilder->getQuery()->execute();

        $countPhqlBuilder = $phqlQueryParser->fromQuery( $query);
        $phqlBuilder->from( 'Recipe');
        $phqlBuilder->columns( 'count(*) as count');
        
        $resultCountRecipes = $phqlBuilder->getQuery()->execute();
        $countRecipesValue = $resultCountRecipes->getFirst();
        $this->response->setHeader('Total-Count', $countRecipesValue->count);

        return $this->respondCollection( $recipes, new RecipeTransformer(), 'recipes');
    }

    public function find( $recipeId)
    {
        $recipe = Recipe::findFirst((int)$recipeId);

        if (!$recipe) {
            throw new UserException(ErrorCodes::DATA_NOTFOUND, 'Recipe with id: #' . (int)$recipeId . ' could not be found.');
        }

        return $this->respondItem($recipe, new RecipeTransformer, 'recipe');
    }

    // /**
    //  * @title("Authenticate")
    //  * @description("Authenticate user")
    //  * @headers({
    //  *      "Authorization": "'Basic sd9u19221934y='"
    //  * })
    //  * @requestExample("POST /users/authenticate")
    //  * @response("Data object or Error object")
    //  */
    // public function authenticate()
    // {
    //     $username = $this->request->getUsername();
    //     $password = $this->request->getPassword();

    //     $session = $this->authManager->loginWithUsernamePassword(\App\Auth\UsernameAccountType::NAME, $username, $password);
    //     $response = [
    //         'token' => $session->getToken(),
    //         'expires' => $session->getExpirationTime()
    //     ];

    //     return $this->respondArray($response, 'data');
    // }
}
