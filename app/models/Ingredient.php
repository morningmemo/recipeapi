<?php

class Ingredient extends \App\Mvc\Model
{
    public function columnMap()
    {
        return [
            'id' => 'id',
            'recipe_id' => 'recipeId',
            'ingredient' => 'ingredient',
            'amount' => 'amount',
            'created' => 'created',
            'modified' => 'modified'
        ];
    }

    public function whitelist()
    {
        return [
            'id',
            'recipeId',
            'ingredient',
            'amount',
            'created',
            'modified'
        ];
    }

    public function getSource()
    {
        return 'ingredient';
    }
}