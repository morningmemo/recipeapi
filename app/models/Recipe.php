<?php

class Recipe extends \App\Mvc\Model
{
    public function initialize()
    {
        $this->hasOne(
            'siteId',
            'site',
            'id',
            array(
                'alias' => 'site'
            )
        );

        $this->hasMany(
            'id',
            'Ingredient',
            'recipeId',
            array(
                'alias' => 'ingredients'
            )
        );
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'site_id' => 'siteId',
            'url' => 'url',
            'nickname' => 'nickname',
            'title' => 'title',
            'main_img' => 'mainImg',
            'created' => 'created',
            'modified' => 'modified',
        ];
    }

    public function whitelist()
    {
        return [
            'id',
            'siteId',
            'url',
            'nickname',
            'title',
            'mainImg',
            'created',
            'modified'
        ];
    }

    public function getSource()
    {
        return 'recipe';
    }
}