<?php

class Site extends \App\Mvc\Model
{
    public $id;
    public $title;
    public $host;

    public function getSource()
    {
        return 'site';
    }

    public function columnMap()
    {
        return [
            'id' => 'id',
            'title' => 'title',
            'host' => 'host'
        ];
    }

    public function whitelist()
    {
        return [
            'title',
            'host'
        ];
    }
}