<?php

use League\Fractal;

class RecipeTransformer extends Fractal\TransformerAbstract
{
    public function transform(\Recipe $recipe)
    {
        $site = $recipe->site;
        return [
            'id' => (int)$recipe->id,
            'site' => array(
                'id' => $site->id,
                'title' => $site->title,
                'host' => $site->host
            ),
            'url' => $recipe->url,
            'nickname' => $recipe->nickname,
            'title' => $recipe->title,
            'mainImg' => $recipe->mainImg,
            'ingredients' => $recipe->ingredients->toArray(),
            'created' => $recipe->created,
            'modified' => $recipe->modified
        ];
    }
}
