<?php

use League\Fractal;

class SiteTransformer extends Fractal\TransformerAbstract
{
    public function transform(\Site $site)
    {
        return [
            'id' => (int)$site->id,
            'site' => $site->title,
            'host' => $site->host
        ];
    }
}
